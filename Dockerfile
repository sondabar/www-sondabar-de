FROM nginx:alpine

# Copy static files into image
COPY ./public /usr/share/nginx/html
