+++
title = "About"
date = 2018-02-18
publishDate = 2018-02-18
description = "Information about me"
draft = false
toc = false
images = [] # overrides the site-wide open graph image
menu = "main"
lastmod = 2019-03-11
+++

## About me

I'm Lars, a software developer from Cologne, Germany. Currently I'm working as a senior software developer at 
[Ligatus][8].

### Languages
- [Java][1]
- [Go][2]
- [PHP][3]
- [Groovy][15]

### Tools
- [GitLab][4]
- [Maven][5]
- [Gradle][6]
- [IntelliJ][7]

### Frameworks
- [Apache Beam][11] 
- [Vert.x][13]
- [Spring Boot][14]

### Previous Employers
- [Telexiom][9] (Consultant: 2009-2011)
- [7P Solutions AG formerly Tecon Systems AG][10] (Consultant: 2007-2008)
- [GGRZ Hagen][12] (Application Developer: 11.2004-10.2006)


## About this blog

This blog is made with [Hugo][16], a static website generator. I am using the [hugo-bootstrap-theme][17]. Thanks to 
the creators of hugo and [Xzya][18] the creator of the wonderful theme. 

[1]: http://www.java.org
[2]: http://www.golang.org
[3]: http://php.net
[4]: https://about.gitlab.com/
[5]: https://mave.apache.org
[6]: https://gradle.org
[7]: https://www.jetbrains.com/idea/
[8]: https://www.ligatus.com/en 
[9]: https://www.telexiom.de/
[10]: https://www.7p-group.com/en/
[11]: https://beam.apache.org
[12]: https://www.it.nrw.de/
[13]: http://vertx.io/
[14]: https://projects.spring.io/spring-boot/
[15]: http://www.groovy-lang.org
[16]: https://gohugo.io
[17]: https://github.com/Xzya/hugo-bootstrap
[18]: https://github.com/Xzya