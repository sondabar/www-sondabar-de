+++
title = "Favicon"
description = "How to easily create a favicon"
date = 2018-03-25
publishdate = 2018-03-25
draft = false
categories = ["blog"]
tags = ["favicon"]
toc = false
+++
www.sondabar.de was missing a [favicon][2] for a while. So I created an image of mine and wanted to use it as the  
sondabar favicon.  After creating the image I asked myself how to it embed in my blog. I came across this nice online
 ["real" favicon generator][1] service which helped me a lot. It generates images and [HTML][3] snippets for all 
 common browsers. Give it a try.

[1]: https://realfavicongenerator.net/
[2]: https://en.wikipedia.org/wiki/Favicon 
[3]: https://en.wikipedia.org/wiki/HTML