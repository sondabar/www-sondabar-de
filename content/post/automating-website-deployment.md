+++
title = "Automating Website Deployment"
description = "Use gitlab and s3_website to automate www.sondabar.de deployment. Push the changes and let the magic happen..."
date = 2018-03-11
lastmod = 2018-03-18
publishdate = 2018-03-11
draft = false
categories = ["static website","blog"]
tags = ["gitlab", "aws", "hugo", "ci", "cd"]
toc = false
+++
It's time to improve the process of deploying new website versions. Currently, the process is done by me without any 
automation. First I write a new blog entry, run [Hugo][1] and at last, I call _aws s3 sync_. That's a good starting 
point but it much better automates this process. I am using [GitLab][2] as my code repository and there is a very 
nice built-in [CI/CD][3] functionality. So let's use it. And I found nice little tool [s3_website][4] for handling 
static websites on S3.

## GitLab CI/CD
[GitLab CI/CD][3] is based on .gitlab-ci.yml that is executed by an GitLab Runner. All this is heavily based on 
[Docker][7]. The .gitlab-ci.yml is similar to a Dockerfile and defines the structure of the build with stages, jobs, 
services, variables and very important image. The Docker image has all required ingredients to build and deploy 
the artifact. 

So what does this mean for the www.sondabar.de pipeline? This project has following dependencies:

* [Hugo][1]
* [s3_website][4]
  * [Ruby][10]
  * [Java 8][11]
  
To get started i created a new [project][12] in GitLab first. Second i created a [Dockerfile][8] for my builder image
. That's the following one. 
{{< highlight Dockerfile "linenos=table" >}}
FROM openjdk:8-jre-slim

LABEL description="Builder and deployer image for the www.sondabar.de website"
LABEL maintainer="lars@cormann.biz"

ENV HUGO_VERSION 0.37.1
ENV HUGO_DEB hugo_${HUGO_VERSION}_Linux-64bit.deb
ENV HUGO_URL https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_DEB}

RUN apt-get update \
 && apt-get dist-upgrade -y \
 && apt-get install ruby wget -y \
 && gem install s3_website \
 && wget ${HUGO_URL} \
 && dpkg -i ${HUGO_DEB} \
 && rm -f ${HUGO_DEB} \
 && rm -rf /tmp/* /var/tmp/* \
 && apt-get -y clean \
 && rm -rf /var/lib/apt/lists/*
{{< / highlight >}}

The base image is the [openjdk:8-jre-slim][13] image because s3_website needs a Java 8 runtime environment. That's 
what you see in line 1. Line 3 and 4 are for some documentation ;) 6-8 are some variables used for the Hugo 
installation. Starting with line 10 the installation process starts. In 10 and 11 [Debian][14] image gets updated to 
the lastest version.  Next line installs [Ruby][10] and [wget][15]. [Ruby][10] is needed by [s3_website][4] and 
[wget][15] is used to download [Hugo][1] in line 14. Next line [Hugo][9] gets installed and the following lines are 
cleaning up of files I don't want in my image.

With this Dockerfile, I can build the docker image I need to build and deploy my website. But there is still 
something missing ;) How can I push build and push the created image to a Docker registry. That's the point the 
[GitLab CI feature][3] joins the game with this simple [.gitlab-ci.yml][5].
{{< highlight yaml "linenos=table" >}}
image: docker:latest

services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay2

stages:
  - build

before_script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com/sondabar/sondabar-builder

build:
  stage: build
  script:
    - docker build -t registry.gitlab.com/sondabar/sondabar-builder .
    - docker push registry.gitlab.com/sondabar/sondabar-builder
{{< / highlight >}}

This configuration uses the latest version of the base Docker image as you can see in line 1. Because we want to build a
 docker image inside of a docker container we need to use the [docker:dind][16] service and I enabled the overlay2 
 docker filesystem driver. This one is faster than the default vfs driver.  Next lines define my pipeline stages. I 
 decided to use only one and named it "build". I'm using the GitLab docker registry and for using it I need to log in
 from the pipeline. That's what's happening in before_script. After login, the image can be build and pushed. That's 
 all and now I'm having a sondabar website builder and deployer image with [Hugo][1] and [s3_website][4].

Next step is using this in [www.sondabar.de project][9] pipeline. That's easy as you can see in the project's 
[.gitlab-ci.yml][6].
{{< highlight yaml "linenos=table" >}}
image: registry.gitlab.com/sondabar/sondabar-builder

variables:
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - pages

pages:
  stage: pages
  script:
  - hugo
  - s3_website push
  artifacts:
    paths:
    - public/
  only:
  - master
{{< / highlight >}}

The [sondabar-builder image][12] described above is used with only one stage "pages". This stage runs only for the 
master branch and it has two commands "hugo" to build the website and "s3_website push" for the deployment. What's 
missing is the s3_website.yml where the configuration for the deployment is set. The secrets of S3 
(AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY) are configured as GitLab secret variables for this pipeline.    
  
That's all and how this article was brought to this page by a merge request for teh master branch.

[1]: https://gohugo.io/
[2]: https://gitlab.com
[3]: https://about.gitlab.com/features/gitlab-ci-cd/
[4]: https://github.com/laurilehmijoki/s3_website
[5]: https://gitlab.com/sondabar/sondabar-builder/blob/7a4c12ee0b153fa695b9b6d6189b7c59ded5d7f0/.gitlab-ci.yml
[6]: https://gitlab.com/sondabar/www-sondabar-de/blob/42cc123c844660ee64b315184051e8be93b6d8b7/.gitlab-ci.yml 
[7]: https://www.docker.com/ 
[8]: https://gitlab.com/sondabar/sondabar-builder/blob/b310bd0dd01978a99cfb30b18224afd3e14744fb/Dockerfile
[9]: https://gitlab.com/sondabar/www-sondabar-de/
[10]: https://www.ruby-lang.org/
[11]: https://beam.apache.org/documentation/sdks/java/
[12]: https://gitlab.com/sondabar/sondabar-builder/
[13]: https://hub.docker.com/_/openjdk/
[14]: https://www.debian.org/index.en.html
[15]: https://www.gnu.org/software/wget/ 
[16]: https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-in-docker-executor               