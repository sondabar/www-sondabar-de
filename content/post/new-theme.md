---
title: "New Theme"
date: 2019-03-11
draft: false
categories : ["blog","static website"]
tags : ["hugo","theme"]
---

After a long time of inactivity it's time to bring back some life into my blog. First step was as you can see to change
the theme from the very dark [after-dark theme][1] to the light [hugo-bootstrap theme][2]. But that is only the 
beginning. More is coming soon to your browser.
 
[1]: https://github.com/comfusion/after-dark
[2]: https://github.com/Xzya/hugo-bootstrap


