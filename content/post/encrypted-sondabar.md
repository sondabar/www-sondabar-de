+++
title = "Encrypted Sondabar"
description = "www.sondabar.de is now delivered by CloudFront with ssl encryption enabled"
date = 2018-03-24
publishdate = 2018-03-24
draft = false
categories = ["static website","blog"]
tags = ["cloudfront", "aws", "ssl", "letsencrypt"]
toc = false
+++
In [2018][5] it's a bad idea to have a blog or website that's not delivered with [SSL/TLS][3] encryption enabled. So I 
that's what I did. I enabled SSL encryption with the help of the Amazon [CDN][4] [CloudFront][2].

The first steps are easy. Create a CloudFront distribution an connect it to S3 bucket. But there was a problem with 
access denied exceptions when I called a subpath of sondabar.de like /about. That was a misconfiguration and I found 
a solution on [stackoverflow][1] and it was very easy. The only change I had to do was to set the "Origin Domain 
Name" under [CloudFront][2] "Origin Settings" to *www.sondabar.de.s3-website.eu-central-1.amazonaws.com*. After this
 sondabar.de was available under the CloudFront URL.
 
Next step was to configure [s3_website.yml][7] to invalidate the CloudFront Cache after a deployment. This happens 
with setting "cloudfront_wildcard_invalidation" in s3_website.yml to true. Also, the "cloudfront_distribution_id" is 
needed which is provided by GitLab as an environment variable.

What's missing is the certificate to encrypt the connections. I decided to use [Let's encrypt][8] because I always 
wanted to use it and make some experiences with. With the three commands below I created my certificate. For this, I 
had to put a challenge on S3 to confirm that I own the website and domain. 

{{< highlight bash >}}
mkdir -p letsencrypt/etc letsencrypt/lib
cd letsencrypt

docker run -it --rm --name letsencrypt \
  -v "${PWD}/etc:/etc/letsencrypt" \
  -v "${PWD}/lib:/var/lib/letsencrypt" \
  quay.io/letsencrypt/letsencrypt:latest \
  certonly --manual -d www.sondabar.de
{{< / highlight >}}

Next upcoming steps are automating this renewal process of the certificate because a [Let's encrypt certificate][8] 
is valid for only 90 days and I don't want to do it manually again. I will give [certbot][10] with the [certot 
s3front plugin][9] a try by integrating it into a GitLab pipeline. 


[1]: https://serverfault.com/questions/581268/amazon-cloudfront-with-s3-access-denied#776143
[2]: https://aws.amazon.com/cloudfront
[3]: https://en.wikipedia.org/wiki/Transport_Layer_Security
[4]: https://en.wikipedia.org/wiki/Content_delivery_network
[5]: https://blog.chromium.org/2018/02/a-secure-web-is-here-to-stay.html
[6]: https://aws.amazon.com/s3
[7]: https://github.com/laurilehmijoki/s3_website
[8]: https://letsencrypt.org
[9]: https://github.com/dlapiduz/certbot-s3front
[10]: https://certbot.eff.org/
