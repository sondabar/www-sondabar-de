+++
title = "Let the blog begin"
date = 2018-02-18
publishDate = 2018-02-18
description = "First blog post"
draft = false
toc = false
categories = ["blog","static website"]
tags = ["hugo","theme", "aws","gitlab","kotlin","kubernetes","beam","flink","kafka","docker"]
lastmod = 2019-03-11
+++

Hi, time for some famous first words. Let the blog begin ;)  

Why the next blog with content nobody is interested in? Because I can. 

So I started with thinking about a very minimalistic toolset to start. I don't want to use such slow and complicated
tool like [WordPress][5]. It's so famous for security issues. I heard a lot about the static website generator
[Hugo][1] and so that's my first try. I picked the [After Dark theme][2] as my [Hugo][1] theme because of it so dark 
like the darcula theme from [IntelliJ IDEA][4] I am using.

Next question I asked myself was, where can I host my static website. I found many blog posts mentioning hosting with
 amazon [S3][21] is very simple. So I give it try. 

The [source code repository][8] is hosted on [GitLab][3] and later I want to use CI feature of [GitLab][3] to automate
my deployment.

## Next steps for the blog

1. Google [announced][6] that Chrome 68 will mark HTTP sites as "not secure". So I have to enable SSL for 
www.sondabar.de. I will give [Amazon CloudFront][7] in combination with a [Let's encrypt][9] certificate a try. 
1. Have a look at [s3_website][16] for deploying my website to S3. 
1. Automating the website deployment process with the [GitLab CI feature][10].  

## Upcoming topics

1. At work, i gained some experience with [Apache Beam][11]. That's a nice Big Data framework with an advanced 
unified programming model which implements batch and streaming data processing jobs that run on any execution engine.
 Beam has SDKs for [Java][17] and [Python][18]. A [Go][19] SDK is in a prototype phase. Beam comes with several 
 integrated runners for [Apex][20], [Flink][14], [Spark][22], [Google Dataflow][23] and [Gearpump][24]. I also like 
 the programming language [Kotlin][12] and I think it's a good fit to Beam with it's [data classes][25]. So some blog
  posts will come with the Beam together with Kotlin topic.
1. [Kubernetes][13] is an interesting technology for container orchestration and I want to use it for some 
experiments with [Apache Beam][11] streaming jobs. With [Apache Flink][14] as runtime environment for the streaming 
job and [Kafka][15] as streaming infrastructure. [Docker][27] has now an [integrated standalone Kubernetes server and
 client][28] so that's a nice starting point for local experiments.  

Stay tuned to see what's going on this blog :)

[1]: https://gohugo.io/
[2]: https://github.com/comfusion/after-dark
[3]: https://gitlab.com
[4]: https://www.jetbrains.com/idea/
[5]: https://wordpress.org
[6]: https://blog.chromium.org/2018/02/a-secure-web-is-here-to-stay.html
[7]: https://aws.amazon.com/cloudfront/
[8]: https://gitlab.com/sondabar/www-sondabar-de
[9]: https://letsencrypt.org/
[10]: https://about.gitlab.com/features/gitlab-ci-cd/
[11]: https://beam.apache.org
[12]: https://kotlinlang.org
[13]: https://kubernetes.io
[14]: https://flink.apache.org/
[15]: https://kafka.apache.org/
[16]: https://github.com/laurilehmijoki/s3_website
[17]: https://beam.apache.org/documentation/sdks/java/
[18]: https://beam.apache.org/documentation/sdks/python/
[19]: https://github.com/apache/beam/tree/master/sdks/go
[20]: http://apex.apache.org/
[22]: http://spark.apache.org/
[23]: https://cloud.google.com/dataflow/
[24]: http://gearpump.apache.org/
[25]: https://kotlinlang.org/docs/reference/data-classes.html
[26]: https://blog.docker.com/2018/01/docker-ee-kubernetes/
[27]: https://www.docker.com/
[28]: https://docs.docker.com/docker-for-mac/kubernetes/
[21]: https://aws.amazon.com/s3

